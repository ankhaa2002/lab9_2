import { SearchBar } from "react-native-elements";
import React, { Component } from "react";
export default class App extends Component {
  state = {
    search: "",
  };
  updateSearch = (search) => {
    this.setState({ search });
  };
  render() {
    const { search } = this.state;
    return (
      <SearchBar
        placeholder=" Search"
        onChangeText={this.updateSearch}
        value={search}
      />
    );
  }
}

export default App;